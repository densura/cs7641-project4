import funcs
import random

import numpy as np

from forest_heatmap import forest_heatmap
from hiivemdptoolbox.hiive.mdptoolbox import example

if __name__ == "__main__":
    random.seed(10)
    np.random.seed(10)
    S = 500
    r1 = 10
    r2 = 2
    wildfire_proba = 0.3
    P, R = example.forest(S, r1, r2, wildfire_proba)
    print(P)
    print(R)
    q_learning_params = dict(
        n_iter=5_000_000,
        alpha=0.01,  # learning rate
        alpha_decay=0.99,
        alpha_min=0.001,
        epsilon=1.0,  # exploration rate
        epsilon_min=0.01,
        epsilon_decay=0.4,
    )

    forest_heatmap(
        S, P, R, (20,25), gamma=0.99, max_iter=10000, q_learning_params=q_learning_params
    )
