import datetime

import hiivemdptoolbox.hiive.mdptoolbox.example as example
import hiivemdptoolbox.hiive.mdptoolbox.mdp as mdp
from gym.envs.toy_text.frozen_lake import generate_random_map

from frozen_lake_heatmap import frozen_lake_heatmap

FROZEN_LAKE_PROBA = 0.7
FROZEN_LAKE_GAMMA = 0.9


def frozen_lake_small():
    q_learning_params = dict(
        n_iter=5_000_000,
        alpha=0.01,  # learning rate
        alpha_decay=0.99,
        alpha_min=0.001,
        epsilon=1.0,  # exploration rate
        epsilon_min=0.01,
        epsilon_decay=0.70,
    )
    size = 4
    random_map = generate_random_map(size=size, p=FROZEN_LAKE_PROBA)
    P, R = example.openai("FrozenLake-v1", desc=random_map, render=True)
    frozen_lake(size, P, R, gamma=FROZEN_LAKE_GAMMA, xlim=30, q_learning_params=q_learning_params)
    frozen_lake_heatmap(
        size=size,
        P=P,
        R=R,
        gamma=FROZEN_LAKE_GAMMA,
        q_learning_params=q_learning_params
    )


def frozen_lake_large():
    q_learning_params = dict(
        n_iter=5_000_000,
        alpha=0.1,
        alpha_decay=0.99,
        alpha_min=0.001,
        epsilon=1.0,
        epsilon_min=0.1,
        epsilon_decay=0.7,
    )

    size = 20
    random_map = generate_random_map(size=size, p=FROZEN_LAKE_PROBA)
    P, R = example.openai("FrozenLake-v1", desc=random_map, render=True)

    frozen_lake(
        size=size,
        P=P,
        R=R,
        gamma=FROZEN_LAKE_GAMMA,
        xlim=40,
        q_learning_params=q_learning_params,
    )

    frozen_lake_heatmap(
        size=size,
        P=P,
        R=R,
        gamma=FROZEN_LAKE_GAMMA,
        q_learning_params=q_learning_params,
    )


def forest_small():
    forest(10, 10, 2, 0.3, epsilon_decay=0.3)


def forest_large():
    forest(500, 10, 2, 0.3, epsilon_decay=0.4)


def forest(S, r1, r2, p, gamma=0.99, qlearning_n_iter=4_000_000, epsilon_decay=0.99, xlim=None):
    file_name = "output/forest_{}-{}.txt".format(S, datetime.datetime.now())
    rewards_file_name = "output/forest_rewards_{}-{}.png".format(
        S, datetime.datetime.now()
    )
    error_file_name = "output/forest_error_{}-{}.png".format(S, datetime.datetime.now())
    reward_state_size_file_name = "output/forest_reward_vs_states_{}-{}.png".format(
        S, datetime.datetime.now()
    )

    P, R = example.forest(S, r1, r2, p)

    print("Running VI")
    vi = mdp.ValueIteration(P, R, gamma)
    vi.run()

    print("Running PI")
    pi = mdp.PolicyIteration(P, R, gamma)
    pi.run()

    print("Running Q")
    q = mdp.QLearning(P, R, gamma, n_iter=qlearning_n_iter, epsilon_decay=epsilon_decay)
    q.run()

    print("Writing Output")
    file = open(file_name, "w", newline="")
    file.write("Forest\n")
    file.write("S={}\n".format(S))
    file.write("r1={}\n".format(r1))
    file.write("r2={}\n".format(r2))
    file.write("p={}\n".format(p))
    output_info(pi, vi, q, file)
    file.close()

    print("Plotting")
    # plot_rewards(
    #     pi,
    #     vi,
    #     q,
    #     "Rewards vs Iteration (Size={})".format(S),
    #     rewards_file_name,
    #     xlim=xlim,
    # )
    # plot_errors(
    #     pi, vi, q, "Error vs Iteration (Size={})".format(S), error_file_name, xlim=xlim
    # )
    meanv_file_name = "output/forest_meanv_{}-{}.png".format(S, datetime.datetime.now())
    plot_mean_values(
        pi,
        vi,
        None,
        "Value vs Iter (S={},r1={},r2={},p={},gamma={})".format(S, r1, r2,p,gamma),
        meanv_file_name,
        xlim=xlim,
    )
    ql_meanv_file_name = "output/forest_meanv_QL_{}-{}.png".format(S, datetime.datetime.now())
    plot_mean_values(
        None,
        None,
        q,
        "Value vs Iter (QL,S={},r1={},r2={},p={},gamma={})".format(S, r1, r2,p,gamma),
        ql_meanv_file_name,
        xlim=None,
    )
    # plot_rewards_vs_state_size(
    #     q, "Reward vs State Size (Size={})".format(S), reward_state_size_file_name
    # )


def output_info(pi, vi, q, file):
    file.write("== PI Time\n")
    file.write(f"{pi.time}\n")
    file.write("== VI Time\n")
    file.write(f"{vi.time}\n")
    file.write("== QLearning Time\n")
    file.write(f"{q.time}\n\n")

    file.write("== Q\n")
    file.write(f"{str(q.Q)}\n\n")

    file.write("== PI Max Iter\n")
    file.write(f"{pi.max_iter}\n")
    file.write("== VI Max Iter\n")
    file.write(f"{vi.max_iter}\n\n")
    file.write("== QLearning Max Iter\n")
    file.write(f"{q.max_iter}\n\n")

    file.write("== PI Policy\n")
    file.write(str(pi.policy) + "\n")
    file.write("== VI Policy\n")
    file.write(str(vi.policy) + "\n")
    file.write("== QLearning Policy\n")
    file.write(str(q.policy) + "\n\n")

    file.write("== PI Run Stat Frequency\n")
    for stat in pi.run_stats:
        file.write(str(stat))
        file.write("\n")
    file.write("\n")

    file.write("== VI Run Stat Frequency\n")
    for stat in vi.run_stats:
        file.write(str(stat))
        file.write("\n")
    file.write("\n")

    file.write("== QLearning Run Stat Frequency\n")
    for stat in q.run_stats:
        file.write(str(stat))
        file.write("\n")
    file.write("\n")

    file.close()


def frozen_lake(size, P, R, gamma=0.9, xlim=None, q_learning_params={}):
    size2 = size * size
    file_name = "output/frozen_lake_{}-{}.txt".format(size, datetime.datetime.now())
    epsilon_decay_label = int(q_learning_params["epsilon_decay"] * 100)
    rewards_file_name = "output/frozen_lake_rewards_e{}-{}.png".format(
        size2, epsilon_decay_label, datetime.datetime.now()
    )
    error_file_name = "output/frozen_lake_error_e{}_{}-{}.png".format(
        size2, epsilon_decay_label, datetime.datetime.now()
    )
    meanv_file_name = "output/frozen_lake_meanv_e{}_{}-{}.png".format(
        size2, epsilon_decay_label, datetime.datetime.now()
    )
    maxv_file_name = "output/frozen_lake_maxv_e{}_{}-{}.png".format(
        size2, epsilon_decay_label, datetime.datetime.now()
    )

    print("Generating Frozen Lake")

    print("Running VI")
    vi = mdp.ValueIteration(P, R, gamma, run_stat_frequency=1)
    vi.run()

    print("Running PI")
    pi = mdp.PolicyIteration(P, R, gamma, run_stat_frequency=1)
    pi.run()

    print("Running QLearning")
    q = mdp.QLearning(P, R, gamma, **q_learning_params, run_stat_frequency=1)
    q.run()

    print("Writing Output")
    file = open(file_name, "w", newline="")
    file.write("Frozen Lake\n")
    file.write("Size={}\n".format(size))
    output_info(pi, vi, q, file)
    file.close()

    print("Plotting")
    plot_rewards(
        pi,
        vi,
        None,
        "Rewards vs Iteration (Size={})".format(size2),
        rewards_file_name,
        xlim=xlim,
    )
    # plot_errors(
    #     pi,
    #     vi,
    #     None,
    #     "Error vs Iteration (Size={})".format(size2),
    #     error_file_name,
    #     xlim=xlim,
    # )
    plot_mean_values(
        pi,
        vi,
        None,
        "Mean Value vs Iteration (Size={})".format(size2),
        meanv_file_name,
        xlim=xlim,
    )
    # plot_max_values(
    #     pi,
    #     vi,
    #     None,
    #     "Max Value vs Iteration (Size={})".format(size2),
    #     maxv_file_name,
    #     xlim=xlim,
    # )

    # QLearning
    ql_rewards_file_name = "output/frozen_lake_rewards_ql_{}-{}.png".format(
        size, datetime.datetime.now()
    )
    ql_error_file_name = "output/frozen_lake_error_ql_{}-{}.png".format(
        size, datetime.datetime.now()
    )
    ql_meanv_file_name = "output/frozen_lake_meanv_ql_{}-e{}-{}.png".format(
        size, epsilon_decay_label, datetime.datetime.now()
    )
    ql_maxv_file_name = "output/frozen_lake_maxv_ql_{}-{}.png".format(
        size, datetime.datetime.now()
    )

    # plot_rewards(
    #     None,
    #     None,
    #     q,
    #     "Rewards vs Iteration (QLearning, Size={})".format(size2),
    #     ql_rewards_file_name,
    #     xlim=None,
    # )
    # plot_errors(
    #     None,
    #     None,
    #     q,
    #     "Error vs Iteration (QLearning, Size={})".format(size2),
    #     ql_error_file_name,
    #     xlim=None,
    # )
    plot_mean_values(
        None,
        None,
        q,
        "Mean Value vs Iteration (QLearning, Size={})".format(size2),
        ql_meanv_file_name,
        xlim=None,
    )
    # plot_max_values(
    #     None,
    #     None,
    #     q,
    #     "Max Value vs Iteration (QLearning, Size={})".format(size2),
    #     ql_maxv_file_name,
    #     xlim=None,
    # )

    reward_state_size_file_name = (
        "output/frozen_lake_QL_reward_vs_states_{}-e{}-{}.png".format(
            size2, epsilon_decay_label, datetime.datetime.now()
        )
    )
    plot_rewards_vs_state_size(
        q, "State Size vs Iteration (QLearning, Size={})".format(size2), reward_state_size_file_name, xlim=1000
    )

    print("Done")


def plot_mean_values(pi, vi, q, title, file_path, xlim=None):
    import matplotlib.pyplot as plt

    plt.clf()

    if pi is not None:
        iterations = [stat["Iteration"] for stat in pi.run_stats]
        pi_data = [stat["Mean V"] for stat in pi.run_stats]
        plt.plot(iterations, pi_data, label="PI Mean Value", color="r")

    if vi is not None:
        iterations = [stat["Iteration"] for stat in vi.run_stats]
        vi_data = [stat["Mean V"] for stat in vi.run_stats]
        plt.plot(iterations, vi_data, label="VI Mean Value", color="g")

    if q is not None:
        iterations = [stat["Iteration"] for stat in q.run_stats]
        q_data = [stat["Mean V"] for stat in q.run_stats]
        plt.plot(iterations, q_data, label="QLearning Mean Value", color="b")

    plt.xlabel("Iteration")
    plt.ylabel("Mean Value")
    plt.legend(loc="best")
    if xlim:
        plt.xlim(0, xlim)
    plt.title(title)
    plt.savefig(file_path, format="png")


def plot_max_values(pi, vi, q, title, file_path, xlim=None):
    import matplotlib.pyplot as plt

    plt.cla()

    if pi is not None:
        iterations = [stat["Iteration"] for stat in pi.run_stats]
        pi_data = [stat["Max V"] for stat in pi.run_stats]
        plt.plot(iterations, pi_data, label="PI Max Value", color="r")

    if vi is not None:
        iterations = [stat["Iteration"] for stat in vi.run_stats]
        vi_data = [stat["Max V"] for stat in vi.run_stats]
        plt.plot(iterations, vi_data, label="VI Max Value", color="g")

    if q is not None:
        iterations = [stat["Iteration"] for stat in q.run_stats]
        q_data = [stat["Max V"] for stat in q.run_stats]
        plt.plot(iterations, q_data, label="QLearning Max Value", color="b")

    plt.xlabel("Iteration")
    plt.ylabel("Max Value")
    plt.legend(loc="best")
    if xlim:
        plt.xlim(0, xlim)
    plt.title(title)
    plt.savefig(file_path, format="png")


def plot_errors(pi, vi, q, title, file_path, xlim=None):
    import matplotlib.pyplot as plt

    plt.cla()

    if pi is not None:
        iterations = [stat["Iteration"] for stat in pi.run_stats]
        pi_data = [stat["Error"] for stat in pi.run_stats]
        plt.plot(iterations, pi_data, label="PI Error", color="r")

    if vi is not None:
        iterations = [stat["Iteration"] for stat in vi.run_stats]
        vi_data = [stat["Error"] for stat in vi.run_stats]
        plt.plot(iterations, vi_data, label="VI Error", color="g")

    if q is not None:
        iterations = [stat["Iteration"] for stat in q.run_stats]
        q_data = [stat["Error"] for stat in q.run_stats]
        plt.plot(iterations, q_data, label="QLearning Error", color="b")

    plt.xlabel("Iteration")
    plt.ylabel("Error")
    plt.legend(loc="best")
    if xlim:
        plt.xlim(0, xlim)
    plt.title(title)
    plt.savefig(file_path, format="png")


def plot_rewards(pi, vi, q, title, file_path, xlim=None):
    import matplotlib.pyplot as plt

    plt.cla()

    if pi is not None:
        iterations = [stat["Iteration"] for stat in pi.run_stats]
        pi_data = [stat["Reward"] for stat in pi.run_stats]
        plt.plot(iterations, pi_data, label="PI Reward", color="r")

    if vi is not None:
        iterations = [stat["Iteration"] for stat in vi.run_stats]
        vi_data = [stat["Reward"] for stat in vi.run_stats]
        plt.plot(iterations, vi_data, label="VI Reward", color="g")

    if q is not None:
        iterations = [stat["Iteration"] for stat in q.run_stats]
        q_data = [stat["Reward"] for stat in q.run_stats]
        plt.plot(iterations, q_data, label="QLearning Reward", color="b")

    plt.xlabel("Iteration")
    plt.ylabel("Reward")
    plt.legend(loc="best")
    if xlim:
        plt.xlim(0, xlim)
    plt.title(title)
    plt.savefig(file_path, format="png")


def plot_rewards_vs_state_size(q, title, file_path, xlim=None):
    import matplotlib.pyplot as plt

    plt.cla()

    state_data = [stat["State"] for stat in q.run_stats]
    iterations = [stat["Iteration"] for stat in q.run_stats]
    plt.plot(iterations, state_data, color="r", linewidth=0.5)
    plt.xlabel("Iteration")
    plt.ylabel("State Size")
    # plt.legend(loc="best")
    if xlim:
        plt.xlim(0, xlim)
    plt.title(title)
    plt.savefig(file_path, format="png")
