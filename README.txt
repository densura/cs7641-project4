Account: dsuratna3
Name: Dennis Suratna
Code and dataset are located at: https://gitlab.com/densura/cs7641-project4.git

To clone the code and the dataset, run: `git clone https://gitlab.com/densura/cs7641-project4.git`

The code was written using python 3.9. To install the dependencies, run the following command: `pip3 install -r requirements.txt`

