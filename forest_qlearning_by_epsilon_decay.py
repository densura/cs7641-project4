import random

import numpy as np


def forest_qlearning_exploration_exploitation(
    size, r1, r2, p=0.3, gamma=0.99, n_iter=4_000_000, epsilon_decays=[]
):
    import matplotlib.pyplot as plt
    import datetime
    from hiivemdptoolbox.hiive.mdptoolbox import mdp
    from gym.envs.toy_text.frozen_lake import generate_random_map
    from hiivemdptoolbox.hiive.mdptoolbox import example

    size2 = size

    meanv_filename = "output/forest_QLearningMeanVByDecay_{}-{}.png".format(
        size2, datetime.datetime.now()
    )
    print("Generating Forest")
    P, R = example.forest(size, r1, r2, p)

    q_iterations = []
    q_rewards = []
    q_meanv = []
    q_epsilons = []

    for decay in epsilon_decays:
        print(f"Running QLearning with Decay {decay}")
        q = mdp.QLearning(
            P, R, gamma=gamma, n_iter=n_iter, epsilon_decay=decay, run_stat_frequency=1
        )
        q.run()

        q_iterations.append([stat["Iteration"] for stat in q.run_stats])
        q_rewards.append([stat["Reward"] for stat in q.run_stats])
        q_meanv.append([stat["Mean V"] for stat in q.run_stats])
        q_epsilons.append([stat["Epsilon"] for stat in q.run_stats])

    """
    Plot rewards per iteration (by decay)
    """
    # for idx in range(len(q_rewards)):
    #     plt.cla()
    #     rewards = np.array(q_rewards[idx])
    #     iters = np.array(q_iterations[idx])
    #
    #     non_zeroes_conds = rewards > 0.0
    #     non_zero_rewards = rewards[non_zeroes_conds]
    #     non_zero_iters = iters[non_zeroes_conds]
    #
    #     plt.plot(non_zero_iters, non_zero_rewards, linewidth=0.5)
    #
    #     rewards_filename = "output/frozen_lake_QLearningRewardsByDecay_{}-{}-decay_{}.png".format(
    #         size2, datetime.datetime.now(), int(epsilon_decays[idx] * 100)
    #     )
    #     plt.xlabel("Iteration")
    #     plt.ylabel("Reward")
    #     plt.xlim(0, 1000)
    #     plt.title(f"QLearning Rewards per Iteration (Epsilon Decay={epsilon_decays[idx]})")
    #     plt.savefig(rewards_filename, format="png")

    """
    MeanV By Iteration
    """
    plt.cla()
    for idx in range(len(q_meanv)):
        plt.plot(
            q_iterations[idx],
            q_meanv[idx],
            label=f"(Epsilon Decay={epsilon_decays[idx]})",
            linewidth=1,
        )

    plt.xlabel("Iteration")
    plt.ylabel("MeanV")
    plt.legend(loc="best")
    plt.title("QLearning MeanV per Iteration (by Epsilon decay)")
    plt.savefig(meanv_filename, format="png")

    """
    Epsilon by Iteration
    """
    plt.cla()
    for idx in range(len(q_epsilons)):
        plt.plot(
            q_iterations[idx],
            q_epsilons[idx],
            label=f"(Epsilon Decay={epsilon_decays[idx]})",
            linewidth=1,
        )

    plt.xlabel("Iteration")
    plt.ylabel("Epsilon")
    plt.legend(loc="best")
    plt.title("Epsilon by Iteration")
    plt.xlim(0, 100)
    epsilon_iteration_filename = (
        "output/forest_QLearningEpsilonByIteration_{}-{}.png".format(
            size2, datetime.datetime.now()
        )
    )
    plt.savefig(epsilon_iteration_filename, format="png")


if __name__ == "__main__":
    random.seed(10)
    np.random.seed(10)

    q_learning_params = dict(
        n_iter=100000,
        alpha=0.1,
        alpha_decay=0.99,
        alpha_min=0.001,
        epsilon=1.0,
        epsilon_min=0.02,
    )
    frozen_lake_proba = 0.6
    epsilon_decays = [0.99, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3]

    wild_fire_proba = 0.3
    forest_qlearning_exploration_exploitation(
        10, 10, 2, p=wild_fire_proba, epsilon_decays=epsilon_decays
    )
    forest_qlearning_exploration_exploitation(
        500, 10, 2, p=wild_fire_proba, epsilon_decays=epsilon_decays
    )
