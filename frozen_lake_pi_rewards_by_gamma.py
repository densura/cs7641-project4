import random

import numpy as np


def frozen_lake_pi_plot_rewards_by_gamma(size, p=0.6, max_iter=1000, gammas=[]):
    import matplotlib.pyplot as plt
    import datetime
    from hiivemdptoolbox.hiive.mdptoolbox import mdp
    from gym.envs.toy_text.frozen_lake import generate_random_map
    from hiivemdptoolbox.hiive.mdptoolbox import example

    size2 = size * size
    rewards_filename = "output/frozen_lake_PIRewardsByGamma_{}-{}.png".format(
        size2, datetime.datetime.now()
    )
    meanv_filename = "output/frozen_lake_PIMeanVByGamma_{}-{}.png".format(
        size2, datetime.datetime.now()
    )
    print("Generating Frozen Lake")
    random_map = generate_random_map(size=size, p=p)
    P, R = example.openai("FrozenLake-v1", desc=random_map)

    iterations = []
    rewards = []
    meanvs = []
    times = []

    plt.cla()
    for gamma in gammas:
        print(f"Running PI with Gamma {gamma}")

        pi = mdp.PolicyIteration(P, R, gamma, max_iter=max_iter)
        pi.run()

        iterations.append([stat["Iteration"] for stat in pi.run_stats])
        rewards.append([stat["Reward"] for stat in pi.run_stats])
        meanvs.append([stat["Mean V"] for stat in pi.run_stats])
        times.append([stat["Time"] for stat in pi.run_stats])

    for idx in range(len(rewards)):
        plt.plot(iterations[idx], rewards[idx], label=f"(Gamma={gammas[idx]})")

    plt.xlabel("Iteration")
    plt.ylabel("Reward")
    plt.legend(loc="best")
    plt.title("PI Rewards per Iteration (by Gamma)")
    plt.savefig(rewards_filename, format="png")

    plt.cla()
    for idx in range(len(meanvs)):
        plt.plot(iterations[idx], meanvs[idx], label=f"(Gamma={gammas[idx]})")

    plt.xlabel("Iteration")
    plt.ylabel("MeanV")
    plt.legend(loc="best")
    plt.title("PI MeanV per Iteration (by Gamma)")
    plt.savefig(meanv_filename, format="png")

    plt.cla()
    times_filename = "output/frozen_lake_PITimesVByGamma_{}-{}.png".format(
        size2, datetime.datetime.now()
    )
    for idx in range(len(times)):
        plt.plot(iterations[idx], times[idx], label=f"(Gamma={gammas[idx]})")

    plt.xlabel("Iteration")
    plt.ylabel("Time")
    plt.legend(loc="best")
    plt.title("PI Time per Iteration (by Gamma)")
    plt.savefig(times_filename, format="png")


if __name__ == "__main__":
    random.seed(10)
    np.random.seed(10)
    gammas = [0.9, 0.8, 0.7, 0.5, 0.2]
    frozen_lake_proba = 0.7
    frozen_lake_pi_plot_rewards_by_gamma(
        4, p=frozen_lake_proba, max_iter=10, gammas=gammas
    )
    frozen_lake_pi_plot_rewards_by_gamma(
        20, p=frozen_lake_proba, max_iter=50, gammas=gammas
    )
