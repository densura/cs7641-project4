import random

import numpy as np


def frozen_lake_qlearning_exploration_exploitation(
    size, p=0.6, gamma=0.9, n_iter=1000000, epsilon_decays=[]
):
    import matplotlib.pyplot as plt
    import datetime
    from hiivemdptoolbox.hiive.mdptoolbox import mdp
    from gym.envs.toy_text.frozen_lake import generate_random_map
    from hiivemdptoolbox.hiive.mdptoolbox import example

    size2 = size * size

    meanv_filename = "output/frozen_lake_QLearningMeanVByDecay_{}-{}.png".format(
        size2, datetime.datetime.now()
    )
    print("Generating Frozen Lake")
    random_map = generate_random_map(size=size, p=p)
    P, R = example.openai("FrozenLake-v1", desc=random_map)

    iterations = []
    q_rewards = []
    q_meanv = []
    q_epsilons = []
    times = []

    for decay in epsilon_decays:
        print(f"Running QLearning with Decay {decay}")
        q = mdp.QLearning(
            P, R, gamma=gamma, n_iter=n_iter, epsilon_decay=decay, run_stat_frequency=1,
        )
        q.run()

        print(f"Time:{q.time}")
        iterations.append([stat["Iteration"] for stat in q.run_stats])
        q_rewards.append([stat["Reward"] for stat in q.run_stats])
        q_meanv.append([stat["Mean V"] for stat in q.run_stats])
        q_epsilons.append([stat["Epsilon"] for stat in q.run_stats])
        times.append([stat["Time"] for stat in q.run_stats])

    """
    Plot rewards per iteration (by decay)
    """
    # for idx in range(len(q_rewards)):
    #     plt.cla()
    #     rewards = np.array(q_rewards[idx])
    #     iters = np.array(q_iterations[idx])
    #
    #     non_zeroes_conds = rewards > 0.0
    #     non_zero_rewards = rewards[non_zeroes_conds]
    #     non_zero_iters = iters[non_zeroes_conds]
    #
    #     plt.plot(non_zero_iters, non_zero_rewards, linewidth=0.5)
    #
    #     rewards_filename = "output/frozen_lake_QLearningRewardsByDecay_{}-{}-decay_{}.png".format(
    #         size2, datetime.datetime.now(), int(epsilon_decays[idx] * 100)
    #     )
    #     plt.xlabel("Iteration")
    #     plt.ylabel("Reward")
    #     plt.xlim(0, 1000)
    #     plt.title(f"QLearning Rewards per Iteration (Epsilon Decay={epsilon_decays[idx]})")
    #     plt.savefig(rewards_filename, format="png")

    """
    MeanV By Iteration
    """
    plt.cla()
    for idx in range(len(q_meanv)):
        plt.plot(
            iterations[idx],
            q_meanv[idx],
            label=f"(Epsilon Decay={epsilon_decays[idx]})",
            linewidth=1,
        )

    plt.xlabel("Iteration")
    plt.ylabel("MeanV")
    plt.legend(loc="best")
    plt.title("QLearning MeanV per Iteration (by Epsilon decay)")
    plt.savefig(meanv_filename, format="png")

    """
    Epsilon by Iteration
    """
    # plt.cla()
    # for idx in range(len(q_epsilons)):
    #     plt.plot(
    #         iterations[idx],
    #         q_epsilons[idx],
    #         label=f"(Epsilon Decay={epsilon_decays[idx]})",
    #         linewidth=1,
    #     )
    #
    # plt.xlabel("Iteration")
    # plt.ylabel("Epsilon")
    # plt.legend(loc="best")
    # plt.title("Epsilon by Iteration")
    # plt.xlim(0, 100)
    # epsilon_iteration_filename = (
    #     "output/frozen_lake_QLearningEpsilonByIteration_{}-{}.png".format(
    #         size2, datetime.datetime.now()
    #     )
    # )
    # plt.savefig(epsilon_iteration_filename, format="png")

    """
    Epsilon by Iteration
    """
    # plt.cla()
    # times_filename = "output/frozen_lake_QLearningTimesByIteration_{}-{}.png".format(
    #     size2, datetime.datetime.now()
    # )
    # for idx in range(len(times)):
    #     plt.plot(
    #         iterations[idx], times[idx], label=f"(Epsilon Decay={epsilon_decays[idx]})"
    #     )
    #
    # plt.xlabel("Iteration")
    # plt.ylabel("Time")
    # plt.legend(loc="best")
    # plt.title("QLearning Time per Iteration (by Epsilon Decays)")
    # plt.savefig(times_filename, format="png")


if __name__ == "__main__":
    random.seed(10)
    np.random.seed(10)

    n_iter = 5_000_000
    q_learning_params = dict(
        n_iter=n_iter,
        alpha=0.01,
        alpha_decay=0.9999999,
        alpha_min=0.001,
        epsilon=1.0,
        epsilon_min=0.01,
    )
    frozen_lake_proba = 0.7
    epsilon_decays = [0.99, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3]
    frozen_lake_qlearning_exploration_exploitation(
        4, p=frozen_lake_proba, n_iter=n_iter, epsilon_decays=epsilon_decays
    )
    frozen_lake_qlearning_exploration_exploitation(
        20, p=frozen_lake_proba, n_iter=n_iter, epsilon_decays=epsilon_decays
    )
