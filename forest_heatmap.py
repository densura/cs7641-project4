import datetime

import numpy as np
import seaborn as sns
from gym.envs.toy_text.frozen_lake import generate_random_map

from constants import to_arrows
from hiivemdptoolbox.hiive.mdptoolbox import example, mdp


def forest_heatmap(size, P, R, shape, gamma=0.9, max_iter=10000, q_learning_params={}):
    import matplotlib.pyplot as plt

    pi = mdp.PolicyIteration(P, R, gamma, max_iter=max_iter, run_stat_frequency=1)
    pi.run()
    policy_grid = np.array(pi.policy).reshape(shape)
    value_grid = np.array(pi.V).reshape(shape)
    print(policy_grid)
    print(value_grid)

    plt.clf()
    sns.heatmap(value_grid, annot=policy_grid, linewidths=0.5, fmt="")
    plt.savefig(
        "output/forest_PIHeatMap_{}-{}.png".format(size, datetime.datetime.now())
    )

    vi = mdp.ValueIteration(P, R, gamma, max_iter=max_iter, run_stat_frequency=1)
    vi.run()
    policy_grid = np.array(vi.policy).reshape(shape)
    value_grid = np.array(vi.V).reshape(shape)
    print(policy_grid)
    print(value_grid)

    plt.clf()
    sns.heatmap(value_grid, annot=policy_grid, linewidths=0.5, fmt="")
    plt.savefig(
        "output/forest_VIHeatMap_{}-{}.png".format(size, datetime.datetime.now())
    )

    q = mdp.QLearning(P, R, gamma, **q_learning_params)
    q.run()

    policy_grid = np.array(q.policy).reshape(shape)
    value_grid = np.array(q.V).reshape(shape)
    print(policy_grid)
    print(value_grid)

    plt.clf()
    sns.heatmap(value_grid, annot=policy_grid, linewidths=0.5, fmt="")
    plt.savefig(
        "output/forest_QLHeatMap_{}-{}.png".format(size, datetime.datetime.now())
    )
