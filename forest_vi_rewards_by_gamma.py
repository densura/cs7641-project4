import random

import numpy as np


def forest_vi_plot_rewards_by_gamma(size, r1, r2, p=0.2, gammas=[]):
    import matplotlib.pyplot as plt
    import datetime
    from hiivemdptoolbox.hiive.mdptoolbox import mdp
    from hiivemdptoolbox.hiive.mdptoolbox import example

    size2 = size
    rewards_filename = "output/forest_VIRewardsByGamma_{}-{}.png".format(
        size2, datetime.datetime.now()
    )
    meanv_filename = "output/forest_VIMeanVByGamma_{}-{}.png".format(
        size2, datetime.datetime.now()
    )
    print("Generating Forest")
    P, R = example.forest(size, r1, r2, p)

    vi_iterations = []
    vi_rewards = []
    vi_meanv = []

    plt.cla()
    for gamma in gammas:
        print(f"Running VI with Gamma {gamma}")

        vi = mdp.ValueIteration(
            P, R, gamma, max_iter=100000, epsilon=0.01, run_stat_frequency=1
        )
        vi.run()

        vi_iterations.append([stat["Iteration"] for stat in vi.run_stats])
        vi_rewards.append([stat["Reward"] for stat in vi.run_stats])
        vi_meanv.append([stat["Mean V"] for stat in vi.run_stats])

    # for idx in range(len(vi_rewards)):
    #     plt.plot(vi_iterations[idx], vi_rewards[idx], label=f"(Gamma={gammas[idx]})")
    #
    # plt.xlabel("Iteration")
    # plt.ylabel("Reward")
    # plt.legend(loc="best")
    # plt.title("VI Rewards per Iteration (by Gamma)")
    # plt.savefig(rewards_filename, format="png")

    plt.clf()
    for idx in range(len(vi_meanv)):
        plt.plot(vi_iterations[idx], vi_meanv[idx], label=f"(Gamma={gammas[idx]})")

    plt.xlabel("Iteration")
    plt.ylabel("MeanV")
    plt.legend(loc="best")
    plt.title("VI MeanV per Iteration (by Gamma)")
    plt.savefig(meanv_filename, format="png")


if __name__ == "__main__":
    random.seed(10)
    np.random.seed(10)
    gammas = [0.99, 0.9, 0.8, 0.7, 0.5, 0.2]
    wild_fire_proba = 0.3
    forest_vi_plot_rewards_by_gamma(10, 8, 2, p=wild_fire_proba, gammas=gammas)
    forest_vi_plot_rewards_by_gamma(500, 8, 2, p=wild_fire_proba, gammas=gammas)
