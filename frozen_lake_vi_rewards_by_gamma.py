import random

import numpy as np


def frozen_lake_vi_plot_rewards_by_gamma(size, p=0.6, gammas=[]):
    import matplotlib.pyplot as plt
    import datetime
    from hiivemdptoolbox.hiive.mdptoolbox import mdp
    from gym.envs.toy_text.frozen_lake import generate_random_map
    from hiivemdptoolbox.hiive.mdptoolbox import example

    size2 = size * size
    rewards_filename = "output/frozen_lake_VIRewardsByGamma_{}-{}.png".format(
        size2, datetime.datetime.now()
    )
    meanv_filename = "output/frozen_lake_VIMeanVByGamma_{}-{}.png".format(
        size2, datetime.datetime.now()
    )
    print(f"Generating Frozen Lake Size={size} p={p}")
    random_map = generate_random_map(size=size, p=p)
    P, R = example.openai("FrozenLake-v1", desc=random_map)

    vi_iterations = []
    vi_rewards = []
    vi_meanv = []
    vi_times = []

    plt.cla()
    for gamma in gammas:
        print(f"Running VI with Gamma {gamma}")

        vi = mdp.ValueIteration(
            P, R, gamma, max_iter=10000, epsilon=0.01, run_stat_frequency=1
        )
        vi.run()
        print(f"Max Iter: {vi.max_iter}")

        vi_iterations.append([stat["Iteration"] for stat in vi.run_stats])
        vi_rewards.append([stat["Reward"] for stat in vi.run_stats])
        vi_meanv.append([stat["Mean V"] for stat in vi.run_stats])
        vi_times.append([stat["Time"] for stat in vi.run_stats])

    for idx in range(len(vi_rewards)):
        plt.plot(vi_iterations[idx], vi_rewards[idx], label=f"(Gamma={gammas[idx]})")

    plt.xlabel("Iteration")
    plt.ylabel("Reward")
    plt.legend(loc="best")
    plt.title("VI Rewards per Iteration (by Gamma)")
    plt.savefig(rewards_filename, format="png")

    plt.cla()
    for idx in range(len(vi_meanv)):
        plt.plot(vi_iterations[idx], vi_meanv[idx], label=f"(Gamma={gammas[idx]})")

    plt.xlabel("Iteration")
    plt.ylabel("MeanV")
    plt.legend(loc="best")
    plt.title("VI MeanV per Iteration (by Gamma)")
    plt.savefig(meanv_filename, format="png")

    plt.cla()
    times_filename = "output/frozen_lake_VITimesVByGamma_{}-{}.png".format(
        size2, datetime.datetime.now()
    )
    for idx in range(len(vi_times)):
        plt.plot(vi_iterations[idx], vi_times[idx], label=f"(Gamma={gammas[idx]})")

    plt.xlabel("Iteration")
    plt.ylabel("Time")
    plt.legend(loc="best")
    plt.title("VI Time per Iteration (by Gamma)")
    plt.savefig(times_filename, format="png")


if __name__ == "__main__":
    random.seed(10)
    np.random.seed(10)
    gammas = [0.9, 0.8, 0.7, 0.5, 0.2]
    frozen_lake_proba = 0.7
    frozen_lake_vi_plot_rewards_by_gamma(4, p=frozen_lake_proba, gammas=gammas)
    frozen_lake_vi_plot_rewards_by_gamma(20, p=frozen_lake_proba, gammas=gammas)
