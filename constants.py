FROZEN_LAKE_ACTIONS = {"Left": 0, "Down": 1, "Right": 2, "Up": 3}

FROZEN_LAKE_ACTIONS_MAPPING = ["←", "↓", "←", "↑"]


def to_arrows(arr):
    res = []
    for idx in range(len(arr)):
        res.append(FROZEN_LAKE_ACTIONS_MAPPING[arr[idx]])
    return res
